use crate::at24cx::{self, AT24Cx, ADDR_57};
use crate::ds3231::{self, Alarm2Mode, DateTime, Ds3231, WeekDay};
use crate::ssd1306::{self, OledDisplay, CHAR_W};

use arduino_hal::i2c::I2c;
use bitflags::bitflags;
use core::{mem, slice};
use shared_bus::{BusManagerSimple, I2cProxy, NullMutex};

// EEPROM Stored Data Layout
// address  :      type         :     size
// 0        : [BellsTable; 8]   : 8x64
// 8x64     : cur_table_idx:u8  : 1
// 8x64+1   : is_active : bool  : 1
// 8x64+2   : bell_duration:u8  : 1
// 8x64+3   : magic_number :u8  : 1
//

pub const MAX_TABLE_COUNT: usize = 8;
pub const MAX_BELLS_COUNT: usize = 64 / mem::size_of::<Bell>();
const BELLS_TABL_SIZE: usize = mem::size_of::<BellsTbl>();
//
const TBL_IDX_ADDR: u16 = (MAX_TABLE_COUNT * BELLS_TABL_SIZE) as u16;
const IS_ACTIVE_ADDR: u16 = TBL_IDX_ADDR + 1;
const DURATION_ADDR: u16 = IS_ACTIVE_ADDR + 1;
const MAGIC_ADDR: u16 = DURATION_ADDR + 1;
//
const MAGIC_NUMBER: u8 = 42;

bitflags! {
    #[derive(Default)]
    pub struct OnDays: u8 {
        const SAT = 0b0000_0001;
        const SUN = 0b0000_0010;
        const MON = 0b0000_0100;
        const TUE = 0b0000_1000;
        const WED = 0b0001_0000;
        const THU = 0b0010_0000;
        const FRI = 0b0100_0000;
    }
}
impl OnDays {
    pub fn contain_weekday(self, wday: WeekDay) -> bool {
        match wday {
            WeekDay::Sat => self.contains(OnDays::SAT),
            WeekDay::Sun => self.contains(OnDays::SUN),
            WeekDay::Mon => self.contains(OnDays::MON),
            WeekDay::Tue => self.contains(OnDays::TUE),
            WeekDay::Wed => self.contains(OnDays::WED),
            WeekDay::Thu => self.contains(OnDays::THU),
            WeekDay::Fri => self.contains(OnDays::FRI),
        }
    }
}

#[repr(C)]
#[derive(Debug, Clone, Copy, Default)]
pub struct Bell {
    pub hh: u8,
    pub mm: u8,
    pub on_days: OnDays,
}

#[repr(C)]
#[derive(Debug, Clone, Copy, Default)]
struct BellsTbl {
    tbl_idx: u8,
    bells: [Bell; MAX_BELLS_COUNT],
}

#[repr(u8)]
pub enum OnMode {
    OnSleep,
    OnWakeup,
    OnTimeLost,
    OnDateTime,
    OnDateTimeSetHH,
    OnDateTimeSetMM,
    OnDateTimeSetYear,
    OnDateTimeSetMonth,
    OnDateTimeSetDay,
    OnNextBell,
    OnSetActive(bool),
    OnDuration(u8),
    OnSetDuration(u8),
    OnBellsTblIdx(u8),
    OnSetBellsTblIdx(u8),
    OnBell(Bell, u8),
    OnBellSetHH(Bell, u8),
    OnBellSetMM(Bell, u8),
    OnBellSetOnDays(Bell, u8, WeekDay),
}

type I2cType<'a> = I2cProxy<'a, NullMutex<I2c>>;

pub struct BellManager<'a> {
    rtc: Ds3231<I2cType<'a>>,
    at24c32: AT24Cx<I2cType<'a>, ADDR_57>,
    disp: OledDisplay<I2cType<'a>>,
    bells_tbl: BellsTbl,
    next_bell: Option<DateTime>,
    bell_duration: u8,
    is_active: bool,
    is_dirty: bool,
    cur_temp: u8,
}

impl<'a> BellManager<'a> {
    pub fn new(i2c_bus: &'a BusManagerSimple<I2c>) -> Self {
        let mut bell_mgr = BellManager {
            rtc: ds3231::Ds3231::new(i2c_bus.acquire_i2c()),
            at24c32: at24cx::AT24Cx::new(i2c_bus.acquire_i2c()),
            disp: OledDisplay::new(i2c_bus.acquire_i2c()),
            bells_tbl: Default::default(),
            next_bell: None,
            bell_duration: 0,
            is_active: false,
            is_dirty: false,
            cur_temp: 0,
        };

        bell_mgr.init();

        bell_mgr
    }

    fn write_default_data_to_eeprom(&mut self) {
        for tbl_idx in 0..MAX_TABLE_COUNT {
            self.bells_tbl.tbl_idx = tbl_idx as u8;
            self.save_bell_tbl();
        }
        let data = [0, 0, 0, MAGIC_NUMBER];
        self.at24c32.write_page(TBL_IDX_ADDR, &data).unwrap();
    }

    fn init(&mut self) {
        if self.at24c32.read_byte(MAGIC_ADDR).unwrap() != MAGIC_NUMBER {
            self.write_default_data_to_eeprom();
        }
        self.load_tbl_idx();
        self.load_bell_tbl();
        self.load_state();
        self.load_duration();
        //
        self.rtc.disable_32khz().unwrap();
        self.update();
    }

    pub fn sleep(&mut self) {
        self.update();
        self.disp.sleep();
    }
    pub fn wakeup(&mut self) {
        self.disp.wakeup();

        let (int, _fraq) = self.rtc.temperature().unwrap();
        self.cur_temp = int as u8; // NB: check check model for neg value!
    }

    pub fn is_battery_or_time_issue(&mut self) -> bool {
        self.rtc.has_been_stopped().unwrap()
    }

    fn reorder_bells(&mut self) {
        let bells = &mut self.bells_tbl.bells;
        for i in 0..MAX_BELLS_COUNT {
            for j in 0..MAX_BELLS_COUNT - 1 - i {
                let mut do_swap = false;
                if bells[j].on_days.is_empty() {
                    do_swap = true;
                } else if !bells[j + 1].on_days.is_empty() {
                    if bells[j].hh > bells[j + 1].hh {
                        do_swap = true;
                    } else if (bells[j].hh == bells[j + 1].hh) && (bells[j].mm > bells[j + 1].mm) {
                        do_swap = true;
                    }
                }
                if do_swap {
                    bells.swap(j, j + 1);
                }
            }
        }
    }

    pub fn update(&mut self) {
        self.rtc.disable_alarm2_interrupts().unwrap();
        self.rtc.clear_alarm2_matched_flag().unwrap();
        self.next_bell = None;

        if self.is_dirty {
            self.is_dirty = false;
            self.reorder_bells();
            self.save_bell_tbl();
        }

        self.update_next_bell();
        if let Some(date_time) = self.next_bell {
            self.rtc
                .set_alarm2(date_time, Alarm2Mode::MinutesHourDateMatch)
                .unwrap();
            self.rtc.enable_alarm2_interrupts().unwrap();
        }
    }

    pub fn date_time(&mut self) -> DateTime {
        self.rtc.date_time().unwrap()
    }
    pub fn set_date_time(&mut self, date_time: &DateTime) {
        self.rtc.set_date_time(date_time).unwrap();
        self.update();
    }

    pub fn bell_at(&self, idx: u8) -> Bell {
        self.bells_tbl.bells[idx as usize]
    }
    pub fn set_bell_at(&mut self, bell: Bell, bell_idx: u8) {
        self.bells_tbl.bells[bell_idx as usize] = bell;
        self.is_dirty = true;
    }

    pub fn duration(&self) -> u8 {
        self.bell_duration
    }
    pub fn set_duration(&mut self, duration: u8) {
        self.bell_duration = duration;
        self.save_duration();
    }

    pub fn is_active(&self) -> bool {
        self.is_active
    }
    pub fn set_is_active(&mut self, is_active: bool) {
        self.is_active = is_active;
        self.save_is_active();
    }

    pub fn tbl_idx(&self) -> u8 {
        self.bells_tbl.tbl_idx
    }
    pub fn set_tbl_idx(&mut self, tbl_idx: u8) {
        self.bells_tbl.tbl_idx = tbl_idx;
        self.load_bell_tbl();
        self.save_tbl_idx(tbl_idx);
        self.update();
    }

    //
    fn update_next_bell(&mut self) {
        let now = self.rtc.date_time().unwrap();
        let mut wday = now.weekday();

        for d in 0..8 {
            for b in self.bells_tbl.bells {
                if !b.on_days.contain_weekday(wday) {
                    continue;
                }
                if d != 0 || (b.hh > now.hour()) || (b.hh == now.hour() && b.mm > now.minute()) {
                    let next_bell = now.to_sec2000() + d * 86400
                        - now.hour() as u32 * 3600
                        - now.minute() as u32 * 60
                        + b.hh as u32 * 3600
                        + b.mm as u32 * 60;
                    self.next_bell = DateTime::from_sec2000(next_bell);
                    return;
                }
            }
            wday = wday.next();
        }
    }

    // "   B    "
    // "        "
    pub fn show_battery(&mut self) {
        let mut glyphs = [0_u8; 8];
        self.disp.to(1 * CHAR_W, 127, 5, 7);
        self.disp.draw_glyphs(&glyphs);
        glyphs[3] = ssd1306::ICON_BATTERY;
        self.disp.to(1 * CHAR_W, 127, 1, 3);
        self.disp.draw_glyphs(&glyphs);
    }

    // "HHbMM   "      "   x     "
    // "2023mddD"  or  "         "
    pub fn show_next_bell(&mut self) {
        let icon = if self.is_active {
            ssd1306::ICON_BELL
        } else {
            ssd1306::ICON_X_BELL
        };
        if let Some(date_time) = self.next_bell {
            self.show_set_date_time(&OnMode::OnDateTime, &date_time, false);
            self.disp.to(3 * CHAR_W, 127, 1, 3);
            self.disp.draw_glyphs(&[icon]);
            self.disp.to(6 * CHAR_W, 127, 1, 3);
            self.disp.draw_glyphs(&[0, 0, 0]);
        } else {
            let mut glyphs = [0_u8; 8];
            self.disp.to(1 * CHAR_W, 127, 5, 7);
            self.disp.draw_glyphs(&glyphs);
            glyphs[3] = icon;
            self.disp.to(1 * CHAR_W, 127, 1, 3);
            self.disp.draw_glyphs(&glyphs);
        }
    }

    // "   x     "
    // "         "
    pub fn show_set_active(&mut self, is_active: bool, is_flashing: bool) {
        let icon = if is_active {
            [ssd1306::ICON_BELL]
        } else {
            [ssd1306::ICON_X_BELL]
        };
        self.disp.to(3 * CHAR_W, 127, 1, 3);
        if is_flashing {
            self.disp.draw_glyphs(&[0]);
        } else {
            self.disp.draw_glyphs(&icon);
        }
    }

    // "HH:MM°dd"
    // "20yymddD"
    pub fn show_date_time_degree(&mut self, is_flashing: bool) {
        let date_time = self.date_time();
        self.show_set_date_time(&OnMode::OnDateTime, &date_time, is_flashing);
        let mut glyphs = [0_u8; 3];
        glyphs[0] = ssd1306::DEGREE_C;
        (glyphs[1], glyphs[2]) = Self::digits_code(self.cur_temp);
        self.disp.to(6 * CHAR_W, 127, 1, 3);
        self.disp.draw_glyphs(&glyphs);
    }

    // "HH?MM   "
    // "20yymddD"
    pub fn show_set_date_time(&mut self, mode: &OnMode, date_time: &DateTime, is_flashing: bool) {
        let mut line1 = [0_u8; 5];
        (line1[0], line1[1]) = Self::digits_code(date_time.hour());
        line1[2] = ssd1306::COLON;
        (line1[3], line1[4]) = Self::digits_code(date_time.minute());

        let mut line2 = [0_u8; 8];
        (line2[0], line2[1]) = Self::digits_code(20);
        (line2[2], line2[3]) = Self::digits_code(date_time.y2000());
        line2[4] = date_time.month() + ssd1306::JANUARY - 1;
        (line2[5], line2[6]) = Self::digits_code(date_time.day());
        line2[7] = date_time.weekday_from_saturday() + ssd1306::SATURDAY;
        if is_flashing {
            match mode {
                OnMode::OnDateTime => line1[2] = 0,
                OnMode::OnDateTimeSetHH => (line1[0], line1[1]) = (0, 0),
                OnMode::OnDateTimeSetMM => (line1[3], line1[4]) = (0, 0),
                OnMode::OnDateTimeSetYear => {
                    (line2[0], line2[1], line2[2], line2[3]) = (0, 0, 0, 0)
                }
                OnMode::OnDateTimeSetMonth => line2[4] = 0,
                OnMode::OnDateTimeSetDay => (line2[5], line2[6]) = (0, 0),
                _ => {}
            }
        }
        self.disp.to(1 * CHAR_W, 127, 1, 3);
        self.disp.draw_glyphs(&line1);
        self.disp.to(1 * CHAR_W, 127, 5, 7);
        self.disp.draw_glyphs(&line2);
    }

    // "  sdd:SB"
    // "        "
    pub fn show_set_duration(&mut self, mode: &OnMode, is_flashing: bool) {
        match mode {
            OnMode::OnDuration(duration) => {
                let mut glyphs = [0_u8; 8];
                self.disp.to(1 * CHAR_W, 127, 5, 7);
                self.disp.draw_glyphs(&glyphs);
                //
                let icon = if self.is_active {
                    ssd1306::ICON_BELL
                } else {
                    ssd1306::ICON_X_BELL
                };
                glyphs[2] = ssd1306::SECOND;
                (glyphs[3], glyphs[4]) = Self::digits_code(*duration);
                (glyphs[5], glyphs[6], glyphs[7]) = (ssd1306::COLON, ssd1306::DURATION, icon);

                self.disp.to(1 * CHAR_W, 127, 1, 3);
                self.disp.draw_glyphs(&glyphs);
            }
            OnMode::OnSetDuration(duration) => {
                self.disp.to(4 * CHAR_W, 127, 1, 3);
                if is_flashing {
                    self.disp.draw_glyphs(&[0, 0]);
                } else {
                    self.draw_digits(*duration);
                }
            }
            _ => {}
        }
    }

    // "     dd:P"
    // "         "
    pub fn show_set_tbl_idx(&mut self, mode: &OnMode, is_flashing: bool) {
        match mode {
            OnMode::OnBellsTblIdx(tbl_idx) => {
                let mut glyphs = [0_u8; 8];
                self.disp.to(1 * CHAR_W, 127, 5, 7);
                self.disp.draw_glyphs(&glyphs);
                //
                (glyphs[4], glyphs[5]) = Self::digits_code(*tbl_idx + 1);
                (glyphs[6], glyphs[7]) = (ssd1306::COLON, ssd1306::PROGRAM);
                self.disp.to(1 * CHAR_W, 127, 1, 3);
                self.disp.draw_glyphs(&glyphs);
            }
            OnMode::OnSetBellsTblIdx(tbl_idx) => {
                self.disp.to(5 * CHAR_W, 127, 1, 3);
                if is_flashing {
                    self.disp.draw_glyphs(&[0, 0]);
                } else {
                    self.draw_digits(*tbl_idx + 1);
                }
            }
            _ => {}
        }
    }

    // "hhBmm dd"
    // " DDDDDDD"
    pub fn show_bell(&mut self, bell: &Bell, bell_idx: u8) {
        let icon = if bell.on_days.is_empty() {
            ssd1306::ICON_X_BELL
        } else {
            ssd1306::ICON_BELL
        };
        let mut glyphs = [0_u8; 8];
        (glyphs[0], glyphs[1]) = Self::digits_code(bell.hh);
        glyphs[2] = icon;
        (glyphs[3], glyphs[4]) = Self::digits_code(bell.mm);
        (glyphs[6], glyphs[7]) = Self::digits_code(bell_idx + 1);

        self.disp.to(1 * CHAR_W, 127, 1, 3);
        self.disp.draw_glyphs(&glyphs);
        //
        self.disp.to(2 * CHAR_W, 127, 5, 7);
        let mut wday = WeekDay::Fri;
        for i in (0..7).rev() {
            if bell.on_days.contain_weekday(wday) {
                self.disp.draw_glyphs(&[i + ssd1306::SATURDAY]);
            } else {
                self.disp.draw_glyphs(&[ssd1306::SPACE]);
            }
            wday = wday.prev();
        }
    }

    // " hhBmm dd"
    // "  DDDDDDD"
    pub fn show_set_bell(&mut self, mode: &OnMode, is_flashing: bool) {
        match mode {
            OnMode::OnBellSetHH(bell, _) => {
                self.disp.to(1 * CHAR_W, 127, 1, 3);
                if is_flashing {
                    self.disp.draw_glyphs(&[0, 0]);
                } else {
                    self.draw_digits(bell.hh);
                }
            }
            OnMode::OnBellSetMM(bell, _) => {
                self.disp.to(4 * CHAR_W, 127, 1, 3);
                if is_flashing {
                    self.disp.draw_glyphs(&[0, 0]);
                } else {
                    self.draw_digits(bell.mm);
                }
            }
            OnMode::OnBellSetOnDays(bell, _, wday) => {
                let icon = if bell.on_days.is_empty() {
                    [ssd1306::ICON_X_BELL]
                } else {
                    [ssd1306::ICON_BELL]
                };
                self.disp.to(3 * CHAR_W, 127, 1, 3);
                self.disp.draw_glyphs(&icon);

                //
                let icon = if bell.on_days.contain_weekday(*wday) {
                    [ssd1306::ICON_BELL]
                } else {
                    [ssd1306::ICON_X_BELL]
                };

                let wday_idx = *wday as u8;
                self.disp.to((8 - wday_idx) * CHAR_W, 127, 5, 7);
                if is_flashing {
                    self.disp.draw_glyphs(&icon);
                } else {
                    self.disp.draw_glyphs(&[wday_idx + ssd1306::SATURDAY]);
                }
            }
            _ => {}
        }
    }

    const fn digits_code(n: u8) -> (u8, u8) {
        let (mut d1, mut d0) = (0, n);
        while d0 >= 10 {
            d0 -= 10;
            d1 += 1;
        }
        (d1 + ssd1306::ZERO, d0 + ssd1306::ZERO)
    }

    fn draw_digits(&mut self, n: u8) {
        let mut glyphs = [0_u8; 2];

        (glyphs[0], glyphs[1]) = Self::digits_code(n);
        self.disp.draw_glyphs(&glyphs);
    }

    //
    fn save_is_active(&mut self) {
        self.at24c32
            .write_byte(IS_ACTIVE_ADDR, self.is_active as u8)
            .unwrap();
    }
    fn load_state(&mut self) {
        self.is_active = self.at24c32.read_byte(IS_ACTIVE_ADDR).unwrap() != 0;
    }
    fn save_duration(&mut self) {
        self.at24c32
            .write_byte(DURATION_ADDR, self.bell_duration)
            .unwrap();
    }
    fn load_duration(&mut self) {
        self.bell_duration = self.at24c32.read_byte(DURATION_ADDR).unwrap();
    }
    fn save_tbl_idx(&mut self, tbl_idx: u8) {
        self.at24c32.write_byte(TBL_IDX_ADDR, tbl_idx).unwrap();
    }
    fn load_tbl_idx(&mut self) {
        self.bells_tbl.tbl_idx = self.at24c32.read_byte(TBL_IDX_ADDR).unwrap();
    }
    fn load_bell_tbl(&mut self) {
        let addr = self.bells_tbl.tbl_idx as u16 * BELLS_TABL_SIZE as u16;
        let ptr = ((&mut self.bells_tbl) as *mut BellsTbl) as *mut u8;
        let data = unsafe { slice::from_raw_parts_mut(ptr, BELLS_TABL_SIZE) };
        self.at24c32.read_data(addr, data).unwrap();
    }
    fn save_bell_tbl(&mut self) {
        let addr = self.bells_tbl.tbl_idx as u16 * BELLS_TABL_SIZE as u16;
        let ptr = (&self.bells_tbl as *const BellsTbl) as *const u8;
        let data = unsafe { slice::from_raw_parts(ptr, BELLS_TABL_SIZE) };
        self.at24c32.write_page(addr, &data[..32]).unwrap();
        let addr = addr + 32;
        self.at24c32.write_page(addr, &data[32..]).unwrap();
    }
}
