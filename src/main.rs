#![no_std]
#![no_main]
#![feature(abi_avr_interrupt)]

mod at24cx;
mod bells_manager;
mod ds3231;
mod millis;
mod ssd1306;

use bells_manager::{Bell, OnDays, OnMode};
use ds3231::WeekDay;
use panic_halt as _;

const NO_ACTION_PERIODE: u32 = 32_768;
const DEBOUNCE_PERIODE: u32 = 384;
const CANCEL_PERIODE: u32 = 1024;

enum OnButton {
    OnSelect,
    OnSet,
    None,
}

#[allow(non_snake_case)]
#[avr_device::interrupt(atmega328p)]
fn INT0() {
    // button intr
}

#[allow(non_snake_case)]
#[avr_device::interrupt(atmega328p)]
fn INT1() {
    // rtc intr
}

#[arduino_hal::entry]
fn main() -> ! {
    let dp = arduino_hal::Peripherals::take().unwrap();

    //
    let pins = arduino_hal::pins!(dp);
    let set_button = pins.d6.into_pull_up_input();
    let sel_button = pins.d2.into_pull_up_input();
    let rtc_interrupt = pins.d3.into_pull_up_input();
    let mut relay = pins.a1.into_output();

    //
    millis::init_millis(dp.TC0);

    {
        // enable power down mode
        dp.CPU.smcr.write(|w| w.sm().pdown());
        // INT0 used by button select to wakeup the MCU
        dp.EXINT.eicra.modify(|_, w| w.isc0().val_0x02());
        dp.EXINT.eimsk.modify(|_, w| w.int0().set_bit());
        // INT1 used by RTC to infor MCU about runnig bell
        dp.EXINT.eicra.modify(|_, w| w.isc1().val_0x02());
        dp.EXINT.eimsk.modify(|_, w| w.int1().set_bit());
        unsafe { avr_device::interrupt::enable() };
    }

    // RTC, EEPROM and OLED display sharing the same i2c bus
    let i2c_bus = shared_bus::BusManagerSimple::new(arduino_hal::I2c::new(
        dp.TWI,
        pins.a4.into_pull_up_input(),
        pins.a5.into_pull_up_input(),
        400_000,
    ));
    let mut bells_mgr = bells_manager::BellManager::new(&i2c_bus);

    //
    let mut on_mode = OnMode::OnSleep;
    let mut date_time = bells_mgr.date_time();
    let mut last_action_time = millis::millis();

    loop {
        let t = millis::millis();
        let mut on_button = OnButton::None;

        if (t - last_action_time) > NO_ACTION_PERIODE {
            on_mode = OnMode::OnSleep;
        } else if (t - last_action_time) > DEBOUNCE_PERIODE {
            if sel_button.is_low() {
                while sel_button.is_low() {}

                if millis::millis() - t > CANCEL_PERIODE {
                    bells_mgr.update();
                    on_mode = OnMode::OnDateTime;
                    last_action_time = millis::millis();
                } else {
                    last_action_time = t;
                    on_button = OnButton::OnSelect;
                }
            } else if set_button.is_low() {
                last_action_time = t;
                on_button = OnButton::OnSet;
            }
        }

        //
        match on_mode {
            OnMode::OnSleep => {
                {
                    bells_mgr.sleep();
                    dp.EXINT.eimsk.modify(|_, w| w.int0().set_bit());
                    dp.CPU.smcr.modify(|_, w| w.se().set_bit());
                    avr_device::asm::sleep();
                    dp.CPU.smcr.modify(|_, w| w.se().clear_bit());
                    dp.EXINT.eimsk.modify(|_, w| w.int0().clear_bit());
                    bells_mgr.wakeup();
                }

                if rtc_interrupt.is_low() {
                    bells_mgr.show_next_bell();
                    bells_mgr.update();
                    if bells_mgr.is_active() {
                        relay.set_high();
                        arduino_hal::delay_ms(bells_mgr.duration() as u16 * 1024);
                        relay.set_low();
                    }
                }
                on_mode = OnMode::OnWakeup;
                last_action_time = millis::millis();
            }

            OnMode::OnWakeup => {
                if bells_mgr.is_battery_or_time_issue() {
                    on_mode = OnMode::OnTimeLost;
                    bells_mgr.show_battery();
                } else {
                    on_mode = OnMode::OnDateTime;
                }
            }

            OnMode::OnTimeLost => match on_button {
                OnButton::OnSelect => {
                    on_mode = OnMode::OnDateTime;
                }
                _ => {}
            },

            OnMode::OnDateTime => match on_button {
                OnButton::OnSelect => {
                    on_mode = OnMode::OnNextBell;
                    bells_mgr.show_next_bell();
                }
                OnButton::OnSet => {
                    on_mode = OnMode::OnDateTimeSetHH;
                    date_time = bells_mgr.date_time();
                }
                OnButton::None => {
                    if t & 0x03FF == 0 {
                        let is_flashing = t & 0x07FF == 0;
                        bells_mgr.show_date_time_degree(is_flashing);
                    }
                }
            },

            OnMode::OnDateTimeSetHH => match on_button {
                OnButton::OnSelect => {
                    on_mode = OnMode::OnDateTimeSetMM;
                }
                OnButton::OnSet => {
                    let hh = date_time.hour() + 1;
                    date_time = date_time
                        .with_hour(hh)
                        .unwrap_or(date_time.with_hour(0).unwrap());
                }
                OnButton::None => {
                    if t & 0xFF == 0 {
                        let is_flashing = t & 0x01FF == 0;
                        bells_mgr.show_set_date_time(&on_mode, &date_time, is_flashing);
                    }
                }
            },

            OnMode::OnDateTimeSetMM => match on_button {
                OnButton::OnSelect => {
                    on_mode = OnMode::OnDateTimeSetYear;
                }
                OnButton::OnSet => {
                    let mm = date_time.minute() + 1;
                    date_time = date_time
                        .with_minute(mm)
                        .unwrap_or(date_time.with_minute(0).unwrap());
                }
                OnButton::None => {
                    if t & 0xFF == 0 {
                        let is_flashing = t & 0x01FF == 0;
                        bells_mgr.show_set_date_time(&on_mode, &date_time, is_flashing);
                    }
                }
            },

            OnMode::OnDateTimeSetYear => match on_button {
                OnButton::OnSelect => {
                    on_mode = OnMode::OnDateTimeSetMonth;
                }
                OnButton::OnSet => {
                    let y2000 = if date_time.y2000() > 42 {
                        23
                    } else {
                        date_time.y2000() + 1
                    };

                    date_time = date_time.with_y2000(y2000).unwrap();
                }
                OnButton::None => {
                    if t & 0xFF == 0 {
                        let is_flashing = t & 0x01FF == 0;
                        bells_mgr.show_set_date_time(&on_mode, &date_time, is_flashing);
                    }
                }
            },

            OnMode::OnDateTimeSetMonth => match on_button {
                OnButton::OnSelect => {
                    on_mode = OnMode::OnDateTimeSetDay;
                }
                OnButton::OnSet => {
                    let month = date_time.month() + 1;
                    date_time = date_time
                        .with_day(1)
                        .unwrap()
                        .with_month(month)
                        .unwrap_or(date_time.with_month(1).unwrap());
                }
                OnButton::None => {
                    if t & 0xFF == 0 {
                        let is_flashing = t & 0x01FF == 0;
                        bells_mgr.show_set_date_time(&on_mode, &date_time, is_flashing);
                    }
                }
            },

            OnMode::OnDateTimeSetDay => match on_button {
                OnButton::OnSelect => {
                    bells_mgr.set_date_time(&date_time);
                    on_mode = OnMode::OnDateTime;
                }
                OnButton::OnSet => {
                    let day = date_time.day() + 1;
                    date_time = date_time
                        .with_day(day)
                        .unwrap_or(date_time.with_day(1).unwrap());
                }
                OnButton::None => {
                    if t & 0xFF == 0 {
                        let is_flashing = t & 0x01FF == 0;
                        bells_mgr.show_set_date_time(&on_mode, &date_time, is_flashing);
                    }
                }
            },

            OnMode::OnNextBell => match on_button {
                OnButton::OnSelect => {
                    let duration = bells_mgr.duration();
                    on_mode = OnMode::OnDuration(duration);
                    bells_mgr.show_set_duration(&on_mode, false);
                }
                OnButton::OnSet => {
                    let is_active = bells_mgr.is_active();
                    on_mode = OnMode::OnSetActive(is_active);
                }
                OnButton::None => {}
            },

            OnMode::OnSetActive(is_active) => match on_button {
                OnButton::OnSelect => {
                    bells_mgr.set_is_active(is_active);
                    on_mode = OnMode::OnNextBell;
                    bells_mgr.show_next_bell();
                }
                OnButton::OnSet => {
                    on_mode = OnMode::OnSetActive(!is_active);
                }
                OnButton::None => {
                    if t & 0xFF == 0 {
                        let is_flashing = t & 0x01FF == 0;
                        bells_mgr.show_set_active(is_active, is_flashing);
                    }
                }
            },

            OnMode::OnDuration(duration) => match on_button {
                OnButton::OnSelect => {
                    let tbl_idx = bells_mgr.tbl_idx();
                    on_mode = OnMode::OnBellsTblIdx(tbl_idx);
                    bells_mgr.show_set_tbl_idx(&on_mode, false);
                }
                OnButton::OnSet => {
                    on_mode = OnMode::OnSetDuration(duration);
                }
                OnButton::None => {}
            },

            OnMode::OnSetDuration(duration) => match on_button {
                OnButton::OnSelect => {
                    bells_mgr.set_duration(duration);
                    on_mode = OnMode::OnDuration(duration);
                    bells_mgr.show_set_duration(&on_mode, false);
                }
                OnButton::OnSet => {
                    if duration == 11 {
                        on_mode = OnMode::OnSetDuration(1);
                    } else {
                        on_mode = OnMode::OnSetDuration(duration + 1);
                    }
                }
                OnButton::None => {
                    if t & 0xFF == 0 {
                        let is_flashing = t & 0x01FF == 0;
                        bells_mgr.show_set_duration(&on_mode, is_flashing);
                    }
                }
            },

            OnMode::OnBellsTblIdx(tbl_idx) => match on_button {
                OnButton::OnSelect => {
                    let bell = bells_mgr.bell_at(0);
                    on_mode = OnMode::OnBell(bell, 0);
                    bells_mgr.show_bell(&bell, 0);
                }
                OnButton::OnSet => {
                    on_mode = OnMode::OnSetBellsTblIdx(tbl_idx);
                }
                OnButton::None => {}
            },

            OnMode::OnSetBellsTblIdx(tbl_idx) => match on_button {
                OnButton::OnSelect => {
                    bells_mgr.set_tbl_idx(tbl_idx);
                    on_mode = OnMode::OnBellsTblIdx(tbl_idx);
                    bells_mgr.show_set_tbl_idx(&on_mode, false);
                }
                OnButton::OnSet => {
                    if tbl_idx == (bells_manager::MAX_TABLE_COUNT - 1) as u8 {
                        on_mode = OnMode::OnSetBellsTblIdx(0);
                    } else {
                        on_mode = OnMode::OnSetBellsTblIdx(tbl_idx + 1);
                    }
                }
                OnButton::None => {
                    if t & 0xFF == 0 {
                        let is_flashing = t & 0x01FF == 0;
                        bells_mgr.show_set_tbl_idx(&on_mode, is_flashing);
                    }
                }
            },

            OnMode::OnBell(bell, bell_idx) => match on_button {
                OnButton::OnSelect => {
                    if bell_idx < (bells_manager::MAX_BELLS_COUNT - 1) as u8 {
                        let bell = bells_mgr.bell_at(bell_idx + 1);
                        on_mode = OnMode::OnBell(bell, bell_idx + 1);
                        bells_mgr.show_bell(&bell, bell_idx + 1);
                    } else {
                        on_mode = OnMode::OnDateTime;
                        bells_mgr.update();
                    }
                }
                OnButton::OnSet => {
                    on_mode = OnMode::OnBellSetHH(bell, bell_idx);
                }
                OnButton::None => {}
            },

            OnMode::OnBellSetHH(bell, bell_idx) => match on_button {
                OnButton::OnSelect => {
                    on_mode = OnMode::OnBellSetMM(bell, bell_idx);
                }
                OnButton::OnSet => {
                    let hh = if bell.hh == 23 { 0 } else { bell.hh + 1 };
                    let bell = Bell { hh, ..bell };
                    on_mode = OnMode::OnBellSetHH(bell, bell_idx);
                }
                OnButton::None => {
                    if t & 0xFF == 0 {
                        let is_flashing = t & 0x01FF == 0;
                        bells_mgr.show_bell(&bell, bell_idx);
                        bells_mgr.show_set_bell(&on_mode, is_flashing);
                    }
                }
            },

            OnMode::OnBellSetMM(bell, bell_idx) => match on_button {
                OnButton::OnSelect => {
                    on_mode = OnMode::OnBellSetOnDays(bell, bell_idx, WeekDay::Sat);
                }
                OnButton::OnSet => {
                    let mm = if bell.mm >= 55 { 0 } else { bell.mm + 5 };
                    let bell = Bell { mm, ..bell };
                    on_mode = OnMode::OnBellSetMM(bell, bell_idx);
                }
                OnButton::None => {
                    if t & 0xFF == 0 {
                        let is_flashing = t & 0x01FF == 0;
                        bells_mgr.show_bell(&bell, bell_idx);
                        bells_mgr.show_set_bell(&on_mode, is_flashing);
                    }
                }
            },

            OnMode::OnBellSetOnDays(bell, bell_idx, wday) => match on_button {
                OnButton::OnSelect => {
                    if wday != WeekDay::Fri {
                        on_mode = OnMode::OnBellSetOnDays(bell, bell_idx, wday.next());
                    } else {
                        bells_mgr.set_bell_at(bell, bell_idx);
                        on_mode = OnMode::OnBell(bell, bell_idx);
                        bells_mgr.show_bell(&bell, bell_idx);
                    }
                }
                OnButton::OnSet => {
                    let mut bell = bell;
                    match wday {
                        WeekDay::Sat => bell.on_days.toggle(OnDays::SAT),
                        WeekDay::Sun => bell.on_days.toggle(OnDays::SUN),
                        WeekDay::Mon => bell.on_days.toggle(OnDays::MON),
                        WeekDay::Tue => bell.on_days.toggle(OnDays::TUE),
                        WeekDay::Wed => bell.on_days.toggle(OnDays::WED),
                        WeekDay::Thu => bell.on_days.toggle(OnDays::THU),
                        WeekDay::Fri => bell.on_days.toggle(OnDays::FRI),
                    }
                    on_mode = OnMode::OnBellSetOnDays(bell, bell_idx, wday);
                }
                OnButton::None => {
                    if t & 0xFF == 0 {
                        let is_flashing = t & 0x01FF == 0;
                        bells_mgr.show_bell(&bell, bell_idx);
                        bells_mgr.show_set_bell(&on_mode, is_flashing);
                    }
                }
            },
        }
    }
}
