#![allow(dead_code)]

use embedded_hal::blocking::i2c::{Write, WriteRead};

static DAYS_IN_MONTH: [u8; 11] = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30];

const RTC_ADDR: u8 = 0x68;

struct Register;
impl Register {
    const SECONDS: u8 = 0x00;
    const MINUTES: u8 = 0x01;
    const HOUR: u8 = 0x02;
    const DAY: u8 = 0x03;
    const DATE: u8 = 0x04;
    const MONTH: u8 = 0x05;
    const YEAR: u8 = 0x06;
    const ALARM1: u8 = 0x07;
    const ALARM2: u8 = 0x0B;
    const CONTROL: u8 = 0x0E;
    const STATUS: u8 = 0x0F;
    const TEMP: u8 = 0x11;
}

struct StatusBits;
impl StatusBits {
    const A1F: u8 = 0b0000_0001;
    const A2F: u8 = 0b0000_0010;
    const BSY: u8 = 0b0000_0100;
    const EN32KHZ: u8 = 0b0000_1000;
    const OSF: u8 = 0b1000_0000;
}

struct CtrlBits;
impl CtrlBits {
    const A1IE: u8 = 0b0000_0001;
    const A2IE: u8 = 0b0000_0010;
    const INTCN: u8 = 0b0000_0100;
    const RS1: u8 = 0b0000_1000;
    const RS2: u8 = 0b0001_0000;
    const CONV: u8 = 0b0010_0000;
    const BBSQW: u8 = 0b0100_0000;
    const EOSC: u8 = 0b1000_0000;
}

#[repr(u8)]
pub enum IntSqwMode {
    Off,
    _1Hz,
    _1Khz,
    _4Khz,
    _8Khz,
}

#[repr(u8)]
pub enum Alarm1Mode {
    OncePerSecond,
    SecondMatch,
    SecondMinuteMatch,
    SecondMinuteHourMatch,
    SecoundMinutesHourDateMatch,
    SecoundMinutesHourDayMatch,
}

#[repr(u8)]
pub enum Alarm2Mode {
    OncePerMinute,
    MinuteMatch,
    MinuteHourMatch,
    MinutesHourDateMatch,
    MinutesHourDayMatch,
}

pub struct Ds3231<I2C> {
    i2c: I2C,
}

fn bin2bcd(bin: u8) -> u8 {
    ((bin / 10) << 4) | (bin % 10)
}

fn bcd2bin(bcd: u8) -> u8 {
    (bcd >> 4) * 10 + (bcd & 0xF)
}

impl<I2C, E> Ds3231<I2C>
where
    I2C: Write<Error = E> + WriteRead<Error = E>,
{
    pub fn new(i2c: I2C) -> Self {
        Ds3231 { i2c }
    }

    pub fn set_date_time(&mut self, data_time: &DateTime) -> Result<(), E> {
        let buf = [
            Register::SECONDS,
            bin2bcd(data_time.second()),
            bin2bcd(data_time.minute()),
            bin2bcd(data_time.hour()),
            bin2bcd(data_time.weekday_from_saturday() + 1),
            bin2bcd(data_time.day()),
            bin2bcd(data_time.month()),
            bin2bcd(data_time.y2000()),
        ];

        self.write_data(&buf)?;
        let mut status = self.read_register(Register::STATUS)?;
        status &= !StatusBits::OSF;
        self.write_register(Register::STATUS, status)?;
        Ok(())
    }

    pub fn date_time(&mut self) -> Result<DateTime, E> {
        let mut buf = [0; 8];
        self.read_data(&mut buf)?;
        Ok(DateTime {
            y2000: bcd2bin(buf[7]),
            month: bcd2bin(buf[6] & 0x7F),
            day: bcd2bin(buf[5]),
            hour: bcd2bin(buf[3]),
            minute: bcd2bin(buf[2]),
            second: bcd2bin(buf[1]),
        })
    }

    pub fn has_been_stopped(&mut self) -> Result<bool, E> {
        let status = self.read_register(Register::STATUS)?;
        Ok((status & StatusBits::OSF) != 0)
    }

    pub fn disable_32khz(&mut self) -> Result<(), E> {
        let status = self.read_register(Register::STATUS)?;
        self.write_register(Register::STATUS, status & !StatusBits::EN32KHZ)
    }

    pub fn enable_32khz(&mut self) -> Result<(), E> {
        let status = self.read_register(Register::STATUS)?;
        self.write_register(Register::STATUS, status | StatusBits::EN32KHZ)
    }

    pub fn temperature(&mut self) -> Result<(i8, u8), E> {
        let mut data = [Register::TEMP, 0, 0];
        self.read_data(&mut data)?;
        let int = data[1] as i8;
        let fraq = (data[2] >> 6) * 25;
        Ok((int, fraq))
    }

    pub fn disable_interrupt_output(&mut self) -> Result<(), E> {
        let control = self.read_register(Register::CONTROL)?;
        self.write_register(Register::CONTROL, control & !CtrlBits::INTCN)
    }

    pub fn enable_interrupt_output(&mut self) -> Result<(), E> {
        let control = self.read_register(Register::CONTROL)?;
        self.write_register(Register::CONTROL, control & !CtrlBits::INTCN)
    }

    pub fn set_int_sqw_mode(&mut self, _mode: IntSqwMode) -> Result<(), E> {
        todo!()
    }

    pub fn int_sqw_mode(&mut self, _int_sqw_mode: IntSqwMode) -> Result<IntSqwMode, E> {
        todo!()
    }

    pub fn set_alarm1(&mut self, date_time: DateTime, alarm1_mode: Alarm1Mode) -> Result<(), E> {
        let (a1m1, a1m2, a1m3, a1m4_dydt) = match alarm1_mode {
            Alarm1Mode::OncePerSecond => (0x80, 0x80, 0x80, 0x80),
            Alarm1Mode::SecondMatch => (0, 0x80, 0x80, 0x80),
            Alarm1Mode::SecondMinuteMatch => (0, 0, 0x80, 0x80),
            Alarm1Mode::SecondMinuteHourMatch => (0, 0, 0, 0x80),
            Alarm1Mode::SecoundMinutesHourDateMatch => (0, 0, 0, 0),
            Alarm1Mode::SecoundMinutesHourDayMatch => (0, 0, 0, 0x40),
        };

        let day_date = if a1m4_dydt == 0 {
            date_time.day
        } else {
            date_time.weekday_from_saturday() + 1
        };

        let data = [
            Register::ALARM1,
            bin2bcd(date_time.second()) | a1m1,
            bin2bcd(date_time.minute()) | a1m2,
            bin2bcd(date_time.hour()) | a1m3,
            bin2bcd(day_date) | a1m4_dydt,
        ];

        self.write_data(&data)
    }

    pub fn has_alarm1_fired(&mut self) -> Result<bool, E> {
        let status = self.read_register(Register::STATUS)?;
        Ok((status & StatusBits::A1F) != 0)
    }

    pub fn clear_alarm1_matched_flag(&mut self) -> Result<(), E> {
        let mut status = self.read_register(Register::STATUS)?;
        status &= !StatusBits::A1F;
        self.write_register(Register::STATUS, status)
    }

    pub fn disable_alarm1_interrupts(&mut self) -> Result<(), E> {
        let control = self.read_register(Register::CONTROL)?;
        self.write_register(Register::CONTROL, control & !CtrlBits::A1IE)
    }

    pub fn enable_alarm1_interrupts(&mut self) -> Result<(), E> {
        let control = self.read_register(Register::CONTROL)?;
        self.write_register(Register::CONTROL, control | CtrlBits::A1IE)
    }

    pub fn set_alarm2(&mut self, date_time: DateTime, alarm2_mode: Alarm2Mode) -> Result<(), E> {
        let (a2m2, a2m3, a2m4_dydt) = match alarm2_mode {
            Alarm2Mode::OncePerMinute => (0x80, 0x80, 0x80),
            Alarm2Mode::MinuteMatch => (0, 0x80, 0x80),
            Alarm2Mode::MinuteHourMatch => (0, 0, 0x80),
            Alarm2Mode::MinutesHourDateMatch => (0, 0, 0),
            Alarm2Mode::MinutesHourDayMatch => (0, 0, 0x40),
        };

        let day_date = if a2m4_dydt == 0 {
            date_time.day
        } else {
            date_time.weekday_from_saturday() + 1
        };

        let data = [
            Register::ALARM2,
            bin2bcd(date_time.minute()) | a2m2,
            bin2bcd(date_time.hour()) | a2m3,
            bin2bcd(day_date) | a2m4_dydt,
        ];

        self.write_data(&data)
    }

    pub fn has_alarm2_fired(&mut self) -> Result<bool, E> {
        let status = self.read_register(Register::STATUS)?;
        Ok((status & StatusBits::A2F) != 0)
    }

    pub fn clear_alarm2_matched_flag(&mut self) -> Result<(), E> {
        let mut status = self.read_register(Register::STATUS)?;
        status &= !StatusBits::A2F;
        self.write_register(Register::STATUS, status)
    }

    pub fn disable_alarm2_interrupts(&mut self) -> Result<(), E> {
        let control = self.read_register(Register::CONTROL)?;
        self.write_register(Register::CONTROL, control & !CtrlBits::A2IE)
    }

    pub fn enable_alarm2_interrupts(&mut self) -> Result<(), E> {
        let control = self.read_register(Register::CONTROL)?;
        self.write_register(Register::CONTROL, control | CtrlBits::A2IE)
    }

    //
    fn read_register(&mut self, reg: u8) -> Result<u8, E> {
        let mut buf = [0];
        self.i2c.write_read(RTC_ADDR, &[reg], &mut buf)?;
        Ok(buf[0])
    }

    fn write_register(&mut self, reg: u8, value: u8) -> Result<(), E> {
        let buf = [reg, value];
        self.i2c.write(RTC_ADDR, &buf)
    }

    fn read_data(&mut self, buf: &mut [u8]) -> Result<(), E> {
        self.i2c.write_read(RTC_ADDR, &[buf[0]], &mut buf[1..])
    }

    fn write_data(&mut self, data: &[u8]) -> Result<(), E> {
        self.i2c.write(RTC_ADDR, data)
    }
}

#[repr(u8)]
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum WeekDay {
    Sat,
    Sun,
    Mon,
    Tue,
    Wed,
    Thu,
    Fri,
}
impl WeekDay {
    pub fn next(self) -> WeekDay {
        use WeekDay::*;
        match self {
            Sat => Sun,
            Sun => Mon,
            Mon => Tue,
            Tue => Wed,
            Wed => Thu,
            Thu => Fri,
            Fri => Sat,
        }
    }
    pub fn prev(self) -> WeekDay {
        use WeekDay::*;
        match self {
            Sat => Fri,
            Sun => Sat,
            Mon => Sun,
            Tue => Mon,
            Wed => Tue,
            Thu => Wed,
            Fri => Thu,
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct DateTime {
    y2000: u8,
    month: u8,
    day: u8,
    hour: u8,
    minute: u8,
    second: u8,
}
impl DateTime {
    pub fn new(
        y2000: u8,
        month: u8,
        day: u8,
        hour: u8,
        minute: u8,
        second: u8,
    ) -> Option<DateTime> {
        if month > 12 {
            return None;
        }

        let dt = DateTime {
            y2000,
            month,
            day,
            hour,
            minute,
            second,
        };

        let dt2 = Self::from_sec2000(dt.to_sec2000())?;
        if dt == dt2 {
            return Some(dt);
        }
        None
    }

    pub fn second(&self) -> u8 {
        self.second
    }

    pub fn with_second(&self, second: u8) -> Option<DateTime> {
        if second > 59 {
            return None;
        }
        Some(DateTime { second, ..*self })
    }

    pub fn minute(&self) -> u8 {
        self.minute
    }

    pub fn with_minute(&self, minute: u8) -> Option<DateTime> {
        if minute > 59 {
            return None;
        }
        Some(DateTime { minute, ..*self })
    }

    pub fn hour(&self) -> u8 {
        self.hour
    }

    pub fn with_hour(&self, hour: u8) -> Option<DateTime> {
        if hour > 23 {
            return None;
        }
        Some(DateTime { hour, ..*self })
    }

    pub fn day(&self) -> u8 {
        self.day
    }

    pub fn with_day(&self, day: u8) -> Option<DateTime> {
        let dt = DateTime { day, ..*self };
        let dt2 = Self::from_sec2000(dt.to_sec2000())?;
        if dt == dt2 {
            return Some(dt);
        }
        None
    }

    pub fn month(&self) -> u8 {
        self.month
    }

    pub fn with_month(&self, month: u8) -> Option<DateTime> {
        if month > 12 {
            return None;
        }

        let dt = DateTime { month, ..*self };
        let dt2 = Self::from_sec2000(dt.to_sec2000())?;
        if dt == dt2 {
            return Some(dt);
        }
        None
    }

    pub fn y2000(&self) -> u8 {
        self.y2000
    }

    pub fn with_y2000(&self, y2000: u8) -> Option<DateTime> {
        let dt = DateTime { y2000, ..*self };
        let dt2 = Self::from_sec2000(dt.to_sec2000())?;
        if dt == dt2 {
            return Some(dt);
        }
        None
    }

    pub fn to_days2000(&self) -> u16 {
        let mut days = self.day as u16;

        for i in 1..self.month as usize {
            days += DAYS_IN_MONTH[i - 1] as u16;
        }
        if self.month > 2 && self.y2000 % 4 == 0 {
            days += 1;
        }

        days + 365 * self.y2000 as u16 + (self.y2000 as u16 + 3) / 4 - 1
    }

    pub fn weekday_from_saturday(&self) -> u8 {
        return (self.to_days2000() % 7) as u8;
    }

    pub fn weekday(&self) -> WeekDay {
        match self.weekday_from_saturday() {
            0 => WeekDay::Sat,
            1 => WeekDay::Sun,
            2 => WeekDay::Mon,
            3 => WeekDay::Tue,
            4 => WeekDay::Wed,
            5 => WeekDay::Thu,
            6 => WeekDay::Fri,
            _ => unreachable!(),
        }
    }

    pub fn to_sec2000(&self) -> u32 {
        ((self.to_days2000() as u32 * 24 + self.hour as u32) * 60 + self.minute as u32) * 60
            + self.second as u32
    }

    pub fn from_sec2000(mut t: u32) -> Option<DateTime> {
        let second = (t % 60) as u8;
        t /= 60;
        let minute = (t % 60) as u8;
        t /= 60;
        let hour = (t % 24) as u8;
        let mut days = (t / 24) as u16;

        let mut y2000 = 0;
        let mut is_leap;
        loop {
            is_leap = (y2000 % 4) == 0;
            if is_leap && days >= 366 {
                days -= 366;
                y2000 += 1;
            } else if !is_leap && days >= 365 {
                days -= 365;
                y2000 += 1;
            } else {
                break;
            }
        }
        if y2000 > 99 {
            return None;
        }

        let mut month: u8 = 1;
        while month < 12 {
            let mut dmonth = DAYS_IN_MONTH[month as usize - 1] as u16;
            if is_leap && month == 2 {
                dmonth += 1;
            }
            if days < dmonth {
                break;
            }
            days -= dmonth;
            month += 1;
        }
        let day = (days + 1) as u8;

        Some(DateTime {
            y2000,
            month,
            day,
            hour,
            minute,
            second,
        })
    }
}
