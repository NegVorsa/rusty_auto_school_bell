use embedded_hal::blocking::i2c::{Write, WriteRead};

// TODO better configuration option
#[allow(dead_code)]
pub const ADDR_50: u8 = 0x50;
#[allow(dead_code)]
pub const ADDR_51: u8 = 0x51;
#[allow(dead_code)]
pub const ADDR_52: u8 = 0x52;
#[allow(dead_code)]
pub const ADDR_53: u8 = 0x53;
#[allow(dead_code)]
pub const ADDR_54: u8 = 0x54;
#[allow(dead_code)]
pub const ADDR_55: u8 = 0x55;
#[allow(dead_code)]
pub const ADDR_56: u8 = 0x56;
#[allow(dead_code)]
pub const ADDR_57: u8 = 0x57;

/// AT24Cx Driver
pub struct AT24Cx<I2C, const ADDR: u8> {
    i2c: I2C,
}

impl<I2C, E, const ADDR: u8> AT24Cx<I2C, ADDR>
where
    I2C: Write<Error = E> + WriteRead<Error = E>,
{
    pub fn new(i2c: I2C) -> Self {
        AT24Cx { i2c }
    }

    pub fn write_byte(&mut self, address: u16, byte: u8) -> Result<(), E> {
        let mut buffer = [0; 3];
        buffer[0] = (address >> 8) as u8;
        buffer[1] = address as u8;
        buffer[2] = byte;
        self.i2c.write(ADDR, &buffer)?;
        self.wait();

        Ok(())
    }

    pub fn write_page(&mut self, address: u16, data: &[u8]) -> Result<(), E> {
        let len = core::cmp::min(data.len(), 32);

        let mut buffer = [0; 34];
        buffer[0] = (address >> 8) as u8;
        buffer[1] = address as u8;
        buffer[2..len + 2].clone_from_slice(&data[..len]);

        self.i2c.write(ADDR, &buffer[..data.len() + 2])?;
        self.wait();

        Ok(())
    }

    pub fn read_byte(&mut self, address: u16) -> Result<u8, E> {
        let mut addr = [0; 2];
        addr[0] = (address >> 8) as u8;
        addr[1] = address as u8;
        let mut data = [0; 1];
        self.i2c.write_read(ADDR, &addr, &mut data)?;

        Ok(data[0])
    }

    pub fn read_data(&mut self, address: u16, data: &mut [u8]) -> Result<(), E> {
        let mut addr = [0; 2];
        addr[0] = (address >> 8) as u8;
        addr[1] = address as u8;
        self.i2c.write_read(ADDR, &addr, data)?;

        Ok(())
    }

    fn wait(&mut self) {
        while let Err(_) = self.i2c.write(ADDR, &[]) {}
    }
}
