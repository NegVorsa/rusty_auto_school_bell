`rusty-auto-school-bell`
=======================

this project is a direct conversion of my old C++ arduino project to rust, so its not in rust style (I'm still learning).
![front](images/front.jpg)

You can also look at the [gallery](gallery.md).


## Hardware

The harware used in this project:
 - Arduino nano
 - USB power plug
 - RTC+EEPROM (DS3231 + AT24C32)
 - Coin battery CR20XX
 - I2C OLED Display (SSD1306 128x64)
 - 5v Relay
 - 2 normally open push buttons
 - door bell as a case!

## Wiring

Everything is connected directly to nano board.

Nano -> RTC:
 - A4 -> SDA
 - A5 -> SCL
 - D3 -> SQW
 - 5V -> VCC
 - GND -> GND

Nano -> OLED:
 - A4 -> SDA
 - A5 -> SCK
 - 5V -> VDD
 - GND -> GND

Nano -> Buttons:
 - D6 -> Set button
 - GND -> GND
 - D2 -> Select button
 - GND -> GND

Nano -> Relay input Interface:
 - A1 -> IN
 - 5V -> DC+
 - GND -> DC-

## Pictures
TODO pictures here...

## Features
 - Stand alone unit, no computer required to program the ringing schedule.
 - 8 defferent profiles can be prorgrammed for bell time table.
 - Up to 21 bells time per profiles.
 - Ability to modify bells time table and save the modification into EEPROM

## Build
you should have rust and avr compiler set up correctly and be able to run the blinky example.
visit this [link](https://github.com/Rahix/avr-hal-template)

To build the release version run:

```bash
cargo b -r
```

... connect the arduino nano to flash and run type:

```bash
cargo r -r
```

NB: if you have an issue make sure you have the permission for the port and try for example:

```bash
RAVEDUDE_PORT=/dev/ttyUSB0 cargo r -r
```

where "/dev/ttyUSB0" is the attached port of arduino.

